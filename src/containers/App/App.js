import React, {Component} from 'react';
import './App.css';
import MessageList from "../component/MessageList/MessageList";
import SendingForm from "../component/SendingForm/SendingForm";

const URL = 'http://146.185.154.90:8000/messages';

class App extends Component {

    state = {
        msgList: [],
        message: '',
        author: 'Vlad',
        lastTimeMsg: ''
    };

    getMessages = () => {
        fetch(this.state.lastTimeMsg === '' ? URL : URL + '?datetime=' + this.state.lastTimeMsg).then(res => res.json()).then(value => {
          console.log(value);
          if (value.length > 0) {
                this.setState(prevState => {
                    return prevState.lastTimeMsg = value[value.length - 1].datetime
                });
                this.setState(prevState => {
                    return prevState.msgList = prevState.msgList.concat(value);
                });
            }
        });
    };

    sendMessages = event => {
        event.preventDefault();
        clearInterval(this.interval);

        const form = new URLSearchParams();

        form.append('message', this.state.message);
        form.append('author', this.state.author);
        const config = {
            method: 'POST',
            body: form
        };

        fetch(URL, config).then(() => {
            this.getMessages();
            this.interval = setInterval(this.getMessages, 2000);
        });
    };

    inputHandler = event => {
        this.setState({author: event.target.value})
    };

    textAreaHandler = event => {
        this.setState({message: event.target.value})
    };

    componentDidMount() {
        this.getMessages();
        this.interval = setInterval(this.getMessages, 2000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }


    render() {
        return (
            <div className="App">
                <h3 className='WorkName'>Lab 60 <span>(@Laptev & @Yavorsky) </span></h3>
                <MessageList array={this.state.msgList}/>
                <SendingForm author={this.state.author} message={this.state.message} changeInput={this.inputHandler}
                             changeTextArea={this.textAreaHandler} click={this.sendMessages}/>
            </div>
        );
    }
}

export default App;
