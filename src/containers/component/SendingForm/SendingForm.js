import React from 'react';
import './SendingForm.css';


//на форме отправки есть 2 варианта
//поставить input, и форма будет отправлятся по Enter
// либо поставить textarea и сообщение будет отправлятся по нажатию на SEND, но тогда надо еще добавить БИНД каконить на Enter

const SendingForm = props => {
    return (
        <div>
            <form className='SendingForm' onSubmit={props.click}>
                <input className='AuthorName' type="text" value={props.author} placeholder='Author'
                       onChange={props.changeInput}/>
                <textarea className='TextMessage' type='text'
                          placeholder='Please Enter Message' value={props.message} onChange={props.changeTextArea}/>
                <input className='Button' type="submit" value={'Send'}/>
            </form>
        </div>

    )
};

export default SendingForm;