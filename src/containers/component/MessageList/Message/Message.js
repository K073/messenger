import React from 'react';
import './Message.css';

let options = {
    era: 'long',
    year: 'numeric',
    day: 'numeric',
    weekday: 'long',
    timezone: 'UTC',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
};


const Message = props => {
    return (
        <div className='Message'>
            <div className='title'>
                <div className='Author'>{props.author}</div>
                <div className='Date'>
                    {new Date(props.date).toLocaleString("ru", this.options)}
                </div>
            </div>
            <div className='Mes'>{props.message}</div>
        </div>
    )
};


export default Message;