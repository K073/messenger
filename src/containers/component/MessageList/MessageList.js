import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Message from "./Message/Message";
import './MessageList.css';

class MessageList extends Component {

    componentDidUpdate () {
        const div =  ReactDOM.findDOMNode(this);
        div.scrollTop = div.scrollHeight;
    }
    
    render() {
        return (<div className='MessageList' id="MessageList">
          {this.props.array.map(value =>
            <Message key={value._id} author={value.author} date={value.datetime} message={value.message}/>
          )}
        </div>);
    }
};

export default MessageList;